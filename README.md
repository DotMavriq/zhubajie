# Zhubajie
#### The master of terminal based email scrapers
Zhubajie speaks of `strategies`, `culprits` and `collateral`.

##### Strategies
First you give zhubajie a provided string, a *domain suffix* and a file that it writes to.

**Here's an example:** 
```
zhuba "Åkes Vinäger AB" .se akesvinager.txt
```
The first thing Zhubajie does is determine the many alternative names a given *email suffix* could have from the initial string.
It then adds this to the treatment cue, each one categorized as a `strategy`. 

##### String treatment
*Email suffixes* are usually compacted or contain the words of a company, organization or name separated by hyphens.

Special characters are also replaced with compatible lettering.

```
Åkes Vinäger AB
@akesvinager.se
@akes-vinager.se
@akesvinager-ab.se
@akesvinagerab.se
```
##### Culprits
Each strategy is searched with Google and scraped.
Every email entry becomes listed in `akesvinager.txt`.

Zhubaji proceeds to find email entries that fit a `strategy` and if fitting emails exist, they will be placed as `culprits`.
```
ake@akesvinager.se
```

##### Collateral

It then provides every single email it could find for each search, these are `collateral`, as only a human eye can determine their quality they are marked with a hashtag below the `culprits`.

`collateral` entries are *email suffix agnostic*, so the ones that adhere to the suffix specification will be listed first and others listed below them in `akesvinager.txt`.
```
ake@akesvinager.se
#stenolof@yxmordarna.se
#sten@stanley.com
```

Zhubaji is wise and will additionally provide `collateral` results from the initial string before attempting to look for `culprits` in the `strategies`.
```
#judgingyou@viktvaktarna.se
#osterian@flensosteria.se
#imbehindyou@help.com
ake@akesvinager.se
#stenolof@yxmordarna.se
```

Zhubaji makes sure to only provide unique entries, so *duplicates will be excluded*.

### How to use it
Simple string
```
zhuba "Åkes Vinäger AB" .se akesvinager.txt
```
#### will return:
```
Åkes Vinäger AB (5/5 strategies)                   [###############################] 100%
2 culprits, 10 collateral
```
##### Here's an example of how `akesvinager.txt` could look:
```
#osterian@flensosteria.se
#judgingyou@viktvaktarna.se
#imbehindyou@help.com
ake@akesvinager.se
maria@akesvinager.se
#dirtydeeds@donedirtcheap.se
#akestaxi@taxi.se
#leifgw@polisen.se
#zucc@rberg.anon
#lord@farkwad.biz
#grasshopper@cars.com
#corporation@gmail.com
#stephen@hawking.org
#flynt@stone.rocks
#boils@pota.to
```
### Multiple entries with CSV
```
zhuba allabolag.csv .se allabolag.txt
```
This will take each entry in the chosen CSV-file, treat each entry as a string and execute on each entry in the CSV until they're all parsed. The results will be printed to `allabolag.txt`.

