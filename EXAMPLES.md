Given "Åkes Vinäger AB" we can mark "AB" as trivial since it's
uncommon to include "AB" in domain names. We also provide a
translation table for Swedish letters.

``` sh
zhuba "Åkes Vinäger AB" .se akesvinager.txt --trivial AB --translate ÅÄÖåäö AAOaao
```

This would result in zhuba to consider the following domains.

```
akes-vinagerab.se
akes-vinager.se
akes-vinager-ab.se
akesvinagerab.se
akesvinager.se
```
